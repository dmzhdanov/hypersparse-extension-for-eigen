var searchData=
[
  ['prune',['prune',['../classdcs_1_1_hyper_sparse_matrix.html#a42e49471d64d51ef15d5a96965e2319d',1,'dcs::HyperSparseMatrix::prune(const Scalar &amp;reference, const RealScalar &amp;epsilon=Eigen::NumTraits&lt; RealScalar &gt;::dummy_precision())'],['../classdcs_1_1_hyper_sparse_matrix.html#ac380eac593be7d408ab869aaa89f1916',1,'dcs::HyperSparseMatrix::prune(const KeepFunc &amp;keep=KeepFunc())']]],
  ['pruneindices',['pruneIndices',['../classdcs_1_1_hyper_sparse_matrix.html#a4eb1033e0c5e467f5e4b1b4c218faa7a',1,'dcs::HyperSparseMatrix']]]
];
