var searchData=
[
  ['coeff',['coeff',['../classdcs_1_1_hyper_sparse_matrix.html#aa9ad00432f7314401b6aef2a387b752f',1,'dcs::HyperSparseMatrix']]],
  ['coeffref',['coeffRef',['../classdcs_1_1_hyper_sparse_matrix.html#ac533a7dc3ff96837f6e30e6c653d2856',1,'dcs::HyperSparseMatrix']]],
  ['col',['col',['../classdcs_1_1_hyper_sparse_matrix_1_1_inner_iterator.html#afa1dd30f2c52b8c0b99b0de97b958747',1,'dcs::HyperSparseMatrix::InnerIterator::col()'],['../classdcs_1_1_hyper_sparse_matrix_1_1_reverse_inner_iterator.html#a79d26c534348596a3ccb0a5bb4fad56f',1,'dcs::HyperSparseMatrix::ReverseInnerIterator::col()']]],
  ['cols',['cols',['../classdcs_1_1_hyper_sparse_matrix.html#a0eec59a578765056d9f88e9b23fa8719',1,'dcs::HyperSparseMatrix']]],
  ['conservativeresize',['conservativeResize',['../classdcs_1_1_hyper_sparse_matrix.html#a8d6f7d27deeedbd4fdad91d177debe73',1,'dcs::HyperSparseMatrix::conservativeResize(Eigen::NoChange_t, Index nbCols)'],['../classdcs_1_1_hyper_sparse_matrix.html#a1f5acc3a9ca7d15581c43282f8b58690',1,'dcs::HyperSparseMatrix::conservativeResize(Index nbRows, Eigen::NoChange_t)'],['../classdcs_1_1_hyper_sparse_matrix.html#a14797321813e37eb0f260392631c8a32',1,'dcs::HyperSparseMatrix::conservativeResize(Index nbRows, Index nbCols)']]]
];
