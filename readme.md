# Overview

HyperSparse matrix extension for [Eigen 3.3](https://eigen.tuxfamily.org/dox/) is an implementation of doubly compressed sparse row/column (DCSR(C)) matrix storage scheme.

The DCSR(C) format is originally proposed in:
A. Buluc and J. R. Gilbert "On the representation and multiplication of hypersparse matrices", in IPDPS'08: Proceedings of the 2008 IEEE International Symposium on Parallel and Distributed Processing, IEEE Computer Society, Washington, DC, 2008, pp. 1-11.

This implementation is intended to be maximally compliant with [Eigen::SparseMatrix](https://eigen.tuxfamily.org/dox/classEigen_1_1SparseMatrix.html).

# Documentation

Detailed online documentation is available [here](https://dmzhdanov.bitbucket.io/hypersparse-extension-for-eigen/docs/html/). 

One can also download the pdf manual [here](https://bitbucket.org/dmzhdanov/hypersparse-extension-for-eigen/raw/master/docs/latex/refman.pdf)