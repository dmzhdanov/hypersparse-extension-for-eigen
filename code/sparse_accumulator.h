#ifndef SPARSE_ACCUMULATOR_H
#define SPARSE_ACCUMULATOR_H

namespace util
{
	template<class _Scalar, class _Index>
	class sparse_accumulator
	{
		// data
		typedef std::vector<_Index, Eigen::aligned_allocator<_Index> > IndexContainer;
		typedef char FlagType;
		std::vector<_Scalar, Eigen::aligned_allocator<_Scalar> > values;
		std::vector<FlagType> flags;
		IndexContainer indices;
	public:
		// constructor
		sparse_accumulator(size_t _size = NULL)
		{
			resize(_size);
		};
		// copy constructor
		sparse_accumulator(const sparse_accumulator& other) :values(other.values), flags(other.flags), indices(other.indices)
		{
		};
		// functions
		void resize(size_t _size)
		{
			values.resize(_size);
			flags.resize(_size);
			indices.resize(0);
			indices.reserve(_size);
			memset(flags.data(), 0, sizeof(FlagType/*typename decltype(flags)::value_type*/)*flags.size());
			memset(values.data(), 0, sizeof(_Scalar/*typename decltype(values)::value_type*/)*values.size());
		};

		inline _Index size()
		{
			return _Index(flags.size());
		};

		inline void set_flag(_Index _index)
		{
			if (flags[_index] == 0)
			{
				flags[_index] = 1;
				indices.push_back(_index);
			}
		};

		inline char check_flag(_Index _index)
		{
			return flags[_index];
		};

		void reset_flags()
		{
			for (typename IndexContainer::iterator it = indices.begin(); it != indices.end(); ++it) { flags[*it] = 0; };
			//std::for_each(indices.begin(), indices.end(), [&](_Index i) {flags[i] = 0; });
			indices.resize(0);
		};

		inline void add(_Index _index, _Scalar _value)
		{
			values[_index] += _value;
			set_flag(_index);
		};

		void reset()
		{
			for (typename IndexContainer::iterator it = indices.begin(); it != indices.end(); ++it){flags[*it] = 0; values[*it] = (_Scalar)0;}
			//std::for_each(indices.begin(), indices.end(), [&](_Index i) {flags[i] = 0; values[i] = (_Scalar)0; });
			indices.resize(0);
		};

		void sort_indices()
		{
			std::sort(indices.begin(), indices.end());
		};

		/** \returns the number of non zero coefficients */
		size_t active_size()
		{
			return _Index(indices.size());
		};

		void store(_Index* dest_indices, _Scalar* dest_values)
		{
			memcpy(dest_indices, indices.data(), sizeof(_Index)*indices.size());
			for (typename IndexContainer::iterator it = indices.begin(); it != indices.end(); ++it) { *(dest_values++) = values[*it]; };
			//std::for_each(indices.begin(), indices.end(), [&](_Index _i) {*(dest_values++) = values[_i]; });
		};

		void store_and_reset(_Index* dest_indices, _Scalar* dest_values)
		{
			memcpy(dest_indices, indices.data(), sizeof(_Index)*indices.size());// (&*(indices.end())) - (&*(indices.begin())));
			for (typename IndexContainer::iterator it = indices.begin(); it != indices.end(); ++it){*(dest_values++) = values[*it]; flags[*it] = 0; values[*it] = (_Scalar)0; }
			//std::for_each(indices.begin(), indices.end(), [&](_Index _i) {*(dest_values++) = values[_i]; flags[_i] = 0; values[_i] = (_Scalar)0; });
			indices.resize(0);
		};

		_Scalar& value(_Index index)
		{
			return values[index];
		}

		_Scalar& active_value(_Index active_index)
		{
			return values[indices[active_index]];
		}
	};
};

#endif
